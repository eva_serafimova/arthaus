<?php

use Illuminate\Http\Middleware\HandleCors;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/home', "App\Http\Controllers\RenderController@homePage")->name('home')->middleware('auth');

Route::get('/login', "App\Http\Controllers\RenderController@loginPage")->name('login');

Route::post('/login', "App\Http\Controllers\AuthController@login")->name('login.post');

Route::get('/register', "App\Http\Controllers\RenderController@registrationPage")->name('register');

Route::post('/register', "App\Http\Controllers\AuthController@register")->name('register.post');

Route::post('/sendEmail', "App\Http\Controllers\EmailController@sendEmail")->name('sendEmail');

Route::get('/confirmEmail', "App\Http\Controllers\EmailController@confirmEmail")->name('confirmEmail');

Route::get('/logout', "App\Http\Controllers\AuthController@logout")->name('logout')->middleware('auth');

Route::get('/loginLimitWarning', "App\Http\Controllers\RenderController@loginLimitWarning")->name('loginLimitWarning');