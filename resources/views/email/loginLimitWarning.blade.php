@extends('extension.layout')
@section('title', 'Login Limit Warning')
@section('content')
    <div>
        <p>Someone tried to login to your account</p>
        <a href="{{ route('login') }}">
            <button type="submit" class="btn btn-sm btn-dark rounded-0">visit website</button>
        </a>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
@endsection