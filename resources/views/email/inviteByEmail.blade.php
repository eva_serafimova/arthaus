@extends('extension.layout')
@section('title', 'Activate account')
@section('content')
<div>
    <p>To activate your account, please click on the button below to verify your email address. Once activated, you’ll have full access to our platform.</p>
    <a href="{{ route('confirmEmail', ['email' => $email, 'emailToken' => $emailToken]) }}">
        <button type="submit" class="btn btn-sm btn-dark rounded-0">Click to continue</button>
    </a>
</div>
@endsection