@extends('extension.layout')
@section('title', 'Home')
@section('content')
    @include('include.header')
    <div class="container w-25 my-5 p-2 bg-secondary bg-opacity-10 border d-flex justify-content-center">
        @auth
            <h5>Welcome, {{ auth()->user()->email }}!</h5>
        @endauth
    </div>
@endsection