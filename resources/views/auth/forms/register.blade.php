@extends('extension.layout')
@section('title', 'Send Email')
@section('content')
    @include('include.header')
    <div class="container w-25 my-5 p-2 bg-secondary bg-opacity-10 border">
        @if (session()->has('success'))
            <div class="text-success my-1"> {{ session('success') }} </div>
        @elseif (session()->has('email') && !session()->has('success'))
            <form action="{{ route('register.post') }}" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="email" id="email" value="{{ session('email') }}">
                <div class="mb-3">
                    <label for="password" class="form-label">password</label>
                    <input type="password" class="form-control @if (session()->has('errors.password')) is-invalid @endif" name="password" id="password">
                    @if (session()->has('errors.password'))
                        @foreach (session('errors.password') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endforeach
                    @else
                        <span class="text-warning">Password must containt at least one uppercase letter, a lowercase letter, a number and a special character</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="confirmPassword" class="form-label">confirm password</label>
                    <input type="password" class="form-control @if (session()->has('errors.confirmPassword')) is-invalid @endif" name="confirmPassword" id="confirmPassword">
                    @if (session()->has('errors.confirmPassword'))
                        @foreach (session('errors.confirmPassword') as $error)
                            <span class="text-danger">{{ $error }}</span>
                        @endforeach
                    @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-sm btn-dark rounded-0">Register</button>
                </div>
            </form>
        @else
            <form action="{{ route('sendEmail') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                @if (session()->has('error'))
                    <div class="text-danger my-1"> {{ session('error') }} </div>
                @endif
                <div>
                    <button type="submit" class="btn btn-sm btn-dark rounded-0">Send confirmation email</button>
                </div>
            </form>
        @endif
    </div>
@endsection