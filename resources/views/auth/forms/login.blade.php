@extends('extension.layout')
@section('title', 'Login')
@section('content')
    @include('include.header')
    <div class="container w-25 my-5 p-2 bg-secondary bg-opacity-10 border">
        @if (session()->has('success'))
            <div class="text-success my-1"> {{ session('success') }} </div>
        @endif
        <form action="{{ route('login.post') }}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
                @error('email')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
                @error('password')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div>
                @if (session()->has('error'))
                    <div class="text-danger my-1"> {{ session('error') }} </div>
                @endif
            </div>
            <div>
                <button type="submit" class="btn btn-sm btn-dark rounded-0">Login</button>
            </div>
        </form>
    </div>
@endsection