<nav class="navbar navbar-expand-lg bg-body-tertiary border">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        @auth
          <a class="nav-link @if (request()->route()->getName() === 'home') active @endif" href="{{ route('home') }}">Home</a>
          <a class="nav-link @if (request()->route()->getName() === 'logout') active @endif" href="{{ route('logout') }}">Logout</a>
        @else
          <a class="nav-link @if (request()->route()->getName() === 'login') active @endif" href="{{ route('login') }}">Login</a>
          <a class="nav-link @if (request()->route()->getName() === 'register') active @endif" href="{{ route('register') }}">Register</a>
        @endauth
      </div>
    </div>
  </div>
</nav>