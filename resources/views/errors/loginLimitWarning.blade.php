@extends('extension.layout')
@section('title', 'Login Limit Warning')
@section('content')
    @include('include.header')
    <div class="container w-25 my-5 p-2 bg-secondary bg-opacity-10 border d-flex justify-content-center">
        <p class="alert text-danger">Too many failed attempts, try again later...</p>
    </div>
@endsection