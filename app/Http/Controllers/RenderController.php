<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\{UserService, IpAddressLimitService};


class RenderController extends Controller
{
    public $ipAddressLimitService;
    public $userService;

    public function __construct(
        IpAddressLimitService $ipAddressLimitService,
        UserService $userService
    ) {
        $this->ipAddressLimitService = $ipAddressLimitService;
        $this->userService = $userService;
    }

    public function homePage()
    {
        return view("auth.pages.home");
    }

    public function loginPage(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('home'));
        }

        if ($this->ipAddressLimitService->hasLoginFailedMoreThanThreeTimes($request->ip())) {
            return redirect(route('loginLimitWarning'));
        }

        return view("auth.forms.login");
    }

    public function registrationPage(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('home'));
        }

        if ($this->userService->doesUserExist($request->session()->get('email', ''))) {
            $request->session()->flush();

            $request->session()->flash('success', 'Account has been created successfully!');

            return view("auth.forms.register");
        }

        $request->session()->reflash();

        return view("auth.forms.register");
    }

    public function loginLimitWarning(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('home'));
        }

        if (!$this->ipAddressLimitService->hasLoginFailedMoreThanThreeTimes($request->ip())) {
            return redirect(route('login'));
        }

        return view("errors.loginLimitWarning");
    }
}