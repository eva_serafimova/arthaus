<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\LoginLimitWarning;
use Illuminate\Validation\ValidationException;
use App\Models\{User, EmailInvite, IpAddressLimit};
use Illuminate\Support\Facades\{Auth, Hash, Mail, Session};
use App\Services\{
    IpAddressLimitService,
    EmailInviteService,
    UserService
};
use ErrorException;

class AuthController extends Controller
{
    public $ipAddressLimitService;
    public $emailInviteService;
    public $userService;

    public function __construct(
        IpAddressLimitService $ipAddressLimitService,
        EmailInviteService $emailInviteService,
        UserService $userService
    ) {
        $this->ipAddressLimitService = $ipAddressLimitService;
        $this->emailInviteService = $emailInviteService;
        $this->userService = $userService;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ]);

        $loginCredentials = $request->only('email', 'password');

        if (Auth::attempt($loginCredentials)) {
            return redirect()->intended(route('home'));
        }

        $this->ipAddressLimitService->addFailedLoginAttempt($request->email, $request->ip());

        if ($this->ipAddressLimitService->hasLoginFailedMoreThanThreeTimes($request->ip())) {
            Mail::to($request->email)->send(new LoginLimitWarning());
            return redirect(route('loginLimitWarning'))->with('error', 'Too many failed attempts, try again later.');
        }

        return redirect(route('login'))->withInput()->with('error', 'Invalid email or password.');
    }

    public function register(Request $request)
    {
        $email = $request->get('email', '');
        $emailToken = $request->session()->get('emailToken', '');

        if (!$this->emailInviteService->isRequestValid($email, $emailToken)) {
            $request->session()->flush();
            return redirect(route('register'));
        }

        try {
            $request->validate([
                'email' => 'required|email|unique:users',
                'password' => [
                    'required',
                    'string',
                    'min:10',               // must be at least 10 characters in length
                    'regex:/[a-z]/',        // must contain at least one lowercase letter
                    'regex:/[A-Z]/',        // must contain at least one uppercase letter
                    'regex:/[0-9]/',        // must contain at least one digit
                    'regex:/[@$!%*#?&]/'],  // must contain a special character
                'confirmPassword' => 'required|same:password'
            ]);
        } catch (ValidationException $e) {
            $request->session()->flash('email', $email);
            $request->session()->flash('errors', $e->errors());
            $request->session()->flash('emailToken', $emailToken);

            return redirect(route('register'));
        }

        try {
            $this->userService->createUser($email, $request->password);
        } catch(ErrorException $e) {
            return redirect(route('register'))->with('error', $e->getMessage());
        }
        
        return redirect(route('login'))->with('success', 'Registration successfull, Login to access the application.');
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect(route('login'));
    }
}