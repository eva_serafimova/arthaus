<?php

namespace App\Http\Controllers;

use App\Models\EmailInvite;
use Illuminate\Http\Request;
use App\Mail\ConfirmationEmail;
use App\Services\EmailInviteService;
use ErrorException;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public $emailInviteService;

    public function __construct(
        EmailInviteService $emailInviteService
    ) {
        $this->emailInviteService = $emailInviteService;
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users'
        ]);

        try {
            $emailInvite = $this->emailInviteService->createEmailInvite($request->email);
        } catch (ErrorException $e) {
            return redirect(route('register'))->with('error', $e->getMessage());
        }

        Mail::to($emailInvite->email)->send(new ConfirmationEmail($emailInvite->email, $emailInvite->email_token));

        return redirect(route('register'))->with('success', 'You will recieve an email shortly.');
    }

    public function confirmEmail(Request $request)
    {
        $email = $request->query('email', '');
        $emailToken = $request->query('emailToken', '');

        if (!$this->emailInviteService->isValidEmailInvite($email, $emailToken)) {
            $request->session()->flush();
            return redirect(route('register'));
        }

        $request->session()->flash('email', $email);
        $request->session()->flash('emailToken', $emailToken);

        return redirect(route('register'));
    }
}