<?php

namespace App\Services;

use ErrorException;
use App\Models\EmailInvite;

class EmailInviteService 
{
    public $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    /**
     * @param string $email
     *
     * @throws ErrorException
     * 
     * @return EmailInvite
     */
    public function createEmailInvite(string $email): EmailInvite
    {
        $emailInvite = EmailInvite::firstOrCreate(
            ['email' => $email],
            ['email_token' => $this->generateEmailToken(), 'email_verified_at' => (new \DateTime())]
        );

        if (!$emailInvite) {
            throw new ErrorException('Registration failed, try again later.');
        }

        return $emailInvite;
    }

    /**
     * @param string $email
     * @param string $emailToken
     * 
     * @return bool
     */
    public function isRequestValid(string $email, string $emailToken): bool
    {
        if (
            $this->isValidEmailInvite($email, $emailToken)
            && !$this->userService->doesUserExist($email)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param string $email
     * @param string $emailToken
     * 
     * @return bool
     */
    public function isValidEmailInvite(string $email, string $emailToken): bool
    {
        $emailInvite = EmailInvite::where([['email', $email], ['email_token', $emailToken]])->first();

        if (!$emailInvite) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    private function generateEmailToken(): string
    {
        return bin2hex(random_bytes(10));
    }
}