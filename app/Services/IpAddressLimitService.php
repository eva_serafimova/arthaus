<?php

namespace App\Services;

use App\Models\IpAddressLimit;

class IpAddressLimitService 
{
    /**
     * @param string $email
     * @param string $ipAddress
     * 
     * @return void
     */
    public function addFailedLoginAttempt(string $email, string $ipAddress): void
    {
        IpAddressLimit::create([
            'email' => $email,
            'ip_address' => $ipAddress
        ]);
    }

    /**
     * @param string $ipAddress
     * 
     * @return bool
     */
    public function hasLoginFailedMoreThanThreeTimes(string $ipAddress): bool
    {
        $ipAddressLimit = IpAddressLimit::where([
                ['ip_address', $ipAddress],
                ['created_at', '>=', (new \DateTime('30 minutes ago'))]
            ])
            ->get()
            ->count();

        if ($ipAddressLimit > 3) {
            return true;
        }
    
        return false;
    }
}