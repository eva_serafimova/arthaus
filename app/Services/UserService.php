<?php

namespace App\Services;

use ErrorException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService 
{
    /**
     * @param string $email
     * @param string $password
     * 
     * @throws ErrorException
     * 
     * @return void
     */
    public function createUser(string $email, string $password): void
    {
        $data['email'] = $email;
        $data['password'] = Hash::make($password);
        $user = User::create($data);

        if (!$user) {
            throw new ErrorException('Oops, something went wrong, try again later.');
        }
    }

    /**
     * @param string $email
     * 
     * @return bool
     */
    public function doesUserExist(string $email): bool
    {
        $user = User::where('email', $email)->first();

        if (!$user) {
            return false;
        }

        return true;
    }
}